﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collect : MonoBehaviour
{
    // Start is called before the first frame update
    public int score=0;
    public GameObject collectablesContainer;
    public Text scoreText;
    public Text remainingText;

    void Start()
    {
        updateUIText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Collectable"){
            score+=1;
            other.gameObject.transform.parent=null;
            Destroy(other.gameObject);
            updateUIText();
        }
    }

    private void updateUIText(){
        scoreText.text = "Score: "+score;
        var remaining = collectablesContainer.transform.childCount;
        // Debug.Log("Remaining: "+remaining);
        remainingText.text = "Remaining: "+(remaining);
    }
}
