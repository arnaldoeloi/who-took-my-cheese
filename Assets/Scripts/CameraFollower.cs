﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject objectToFollow;
    public float cameraSpeed=1f;
    public bool shouldLookAtObj=false;
    private Vector3 newCameraPos;
    private Vector3 cameraDiffToObj;

    void Start()
    {
        cameraDiffToObj = transform.position - objectToFollow.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        newCameraPos = objectToFollow.transform.position + cameraDiffToObj;


        transform.position = Vector3.Slerp(transform.position, newCameraPos, Time.deltaTime*cameraSpeed);
        if (shouldLookAtObj) transform.LookAt(objectToFollow.transform);
    }
}
