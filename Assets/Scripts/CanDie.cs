﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanDie : MonoBehaviour
{

    Animator animator;
    CharacterController controller;
    CharacterMovement mover;
    public bool shouldRestartAfterDying;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        mover = GetComponent<CharacterMovement>();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "CanKill"){
            die();
        }
    }

    void die(){
        if(mover != null) {
            mover.enabled = false;
        }


        controller.enabled = false;
        animator.SetBool("isDying", true);
        Invoke("restartScene", 2);
    }

    void restartScene(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
