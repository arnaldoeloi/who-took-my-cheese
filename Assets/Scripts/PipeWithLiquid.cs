﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeWithLiquid : MonoBehaviour
{
    public GameObject colliders;
    public GameObject waterFlow;
    public float onTime  = 2f;
    public float offTime = 3f;

    MeshRenderer waterRenderer;
    

    // Start is called before the first frame update
    void Start()
    {
        waterRenderer = waterFlow.GetComponent<MeshRenderer>();
        enableWaterFlow();
    }

    void enableWaterFlow(){
        colliders.SetActive(true);
        waterRenderer.enabled = true;
        Invoke("disableWaterFlow",onTime);
    }
    
    
    void disableWaterFlow(){
        colliders.SetActive(false);
        waterRenderer.enabled = false;
        Invoke("enableWaterFlow", offTime);
    }
}
