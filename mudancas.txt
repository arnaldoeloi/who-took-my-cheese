Alterações Who Took My Cheese:



1) Melhoria na movimentação de personagem
	Após mais testes de gameplay com outros jogadores, as variáveis de Air Control Percent, Turn Smooth Time e Speed Smooth Time foram alteradas respectivamente para 0.2, 0.06 e 0.1.


2) Correção de bug da movimentação de personagem que o fazia manter o movimento por algum tempo após a tecla de movimento ser liberada.
	Na linha 37 de CharacterMovement o inputDir é passar por parâmetro ao invés de sua versão normalizada. Outrora, a velocidade do personagem era mantida algum tempo após a liberação de tecla de movimento.


3) Correção de bug na fase 2 em que a plataforma possuía colisões instáveis e causava a morte do personagem.
	Após verificar a animação de movimentação da plataforma 1 e sua posição na hierarquia foi descoberto outro objeto animado com as colisões que causava o erro. A animação foi removida e refeita com uma nova plataforma.


4) Sistema de canos de dano melhorado com tempo de entrada e saída.
	Agora existem duas variáveis válidas para o funcionamento dos canos do jogo, uma de entrada e outra de saída. A alteração foi produzida no script de Invoke das funções disableWaterFlow e enableWaterFlow com as novas variáveis onTime e offTime, que indicam, respectivamente, o tempo que a o cano permanece ligado e o tempo que ele permanece desligado. Dessa forma, é possível executar as práticas de Game Design de maneira mais livre.


5) Melhoria na altura das colunas na fase 2 para que fique clara a visualização dos queijos do arranjo horizontal.
	Um problema básico que demonstrou frustração nos jogadores testes foi a altura da terceira plataforma da direita para a esquerda da fase 2. A altura dela foi alterada para que ficasse em um ponto de interesse da câmera do jogador.

 
6) Desempenho do jogo melhorado com alterações no cálculo de Ambient Occlusion (aumento de raio e qualidade direcionada para Medium)
	Testes de desempenho executados com o MSI Afterburner e mudanças nas configurações gráficas demonstraram que o Ambient Occlusion estava ocasionando queda de frames. A sua qualidade foi diminuída para o médio e seu raio aumentado para compensar a diminuição de qualidade. O resultado visual quase não se difere do anterior.


7) Melhoria visual da Fog do cenário 2
	A fog do cenário 2 ficou mais densa para a região central.	


8) Fonte do menu adicionada na interface interna do jogo.
	A fonte utilizada no menu (Painterz) foi a escolhida para definir o estilo e propriedade visual do jogo. No entanto, como não estava na interface do Score não era visível. A mudança foi feita e agora a fonte é a definida.


9) Correção de pivot dos status da UI no Canvas.
	Um erro não encontrado em um momento anterior fazia com que a variável "Remaining" desaparecesse em algumas resoluções. O erro ocorria devido definição incorreta do pivot para esse elemento do Canvas. O pivot foi então movido para o canto inferior esquerdo.	


10) Animação de morte ajustada para movimento de queda sem blend demorado com Idle.
	Utilizando as ferramentas de corte de importação de animações do Unity foram cortados os primeiros 22 frames da animação de morte; que apenas mostrava o personagem parado por aproximadamente um segundo antes de cair. Essa animação retirava parte da imersão no jogo, já que não fazia sentido o personagem travar por alguns segundos de pé antes de cair.
